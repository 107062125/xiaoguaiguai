// 忽略這個

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import moment from 'moment'

Vue.config.productionTip = false;

const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijc1NTVlYmFhMDUwZjY1YmY0N2MxMTYxY2ZiZDI0MDhmM2QxNDk4NGE0MTdhODJlNGNiMGNmN2I3NmJjMjk3OTA0ZGRhMTg1Y2NjMzM0Y2JmIn0.eyJhdWQiOiIyIiwianRpIjoiNzU1NWViYWEwNTBmNjViZjQ3YzExNjFjZmJkMjQwOGYzZDE0OTg0YTQxN2E4MmU0Y2IwY2Y3Yjc2YmMyOTc5MDRkZGExODVjY2MzMzRjYmYiLCJpYXQiOjE1NzcxMTU5NDMsIm5iZiI6MTU3NzExNTk0MywiZXhwIjoxNjA4NzM4MzQzLCJzdWIiOiIyNTYiLCJzY29wZXMiOltdfQ.ZM_eSm7Y1gssVmZoftcGCksUZL57YpZKmdKPCOqH3mtRDGYWoZvmPaC6TsxUSVVWnJZ9qitW3hk75OuM2l_2ZasJtMeISCl_bGz9hmS_kpyJ3-AAZKrmITjdE_JPcgXqFZtDmIs7qZ_CPNlrxJax_k43u0TXFz_jcW87i_fieqJzxlyCkbi--uyiHXN3Dzsq7j_qNlYdCrThQy3AdxK_WelCT9SPfk4KKesWrAnxFshC7vEfbW9MjcgLnhlRBdO1pcGDRC20m4S9Ubywja4cZHw3WF2AjWUbfVrc3YFUZ4Fc-78SEc-9qSaVN-CwMOtA_UOBiLKSs5md8T4RknBMA_5U8eODP5Ueib3x0JoAAZKGWlcTxvvvftDXDwu88BWW9OXT9lqZT8T1W0eSk0EoY_GWspra_7TRUIpk0qpRSZ3HOuwccjg8Ml3j7sZUbxU12I0K1vwVi2pGVWGernArn1YaL2pS3agza0yxs3FWbY5SvickbGYu8uYXo60czV-RCpvmICuJEs2_4tjeFpqkcdDK-fYgv-xeLVYouuFamg62sgrE-_Z3WK_S69TdqjFRaerivPb4ZTpV_pSLmpyeMS-tz2Knxn00crtlgdczbhc-fYEy2uBfg6xwWRvrWvk-vlzsH0toA_rte9Z1fa9ccfwSsH-QQAWj550sP522KTE';
const service = axios.create({
  baseURL: '',
  headers: {
    'Accept': 'application/json',
    'Authorization': 'Bearer ' + token
  },
});

// request interceptor
service.interceptors.request.use(config => {
  if (token) {
    config.headers['Access-Control-Allow-Origin'] = '*';
  }
  return config;
}, error => {
  // Do something with request error
  console.log(error); // for debug
  Promise.reject(error);
});

export default async function api(url, data, method = 'post', params = {}) {
  console.log('api');
  const headers = {};
  headers['Access-Control-Allow-Origin'] = '*';
  console.log(method, url, data, params, headers);
  const result = await service.request({
    method,
    url,
    data,
    params,
    headers
  });
  console.log(result);
  return result.data;
}

Vue.prototype.$moment = moment;
Vue.prototype.$axios = service;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

//fuck verilog